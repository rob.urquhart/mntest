package test240jdk11

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import io.micronaut.core.io.buffer.ByteBuffer
import io.micronaut.core.io.buffer.ReferenceCounted
import io.micronaut.http.*
import io.micronaut.http.client.RxStreamingHttpClient
import io.micronaut.http.client.annotation.Client
import io.reactivex.Flowable

import javax.inject.Inject
import javax.inject.Singleton

@Slf4j
@CompileStatic
@Singleton
class MetadataDownloader {

    @Client('https://caf-shib2ops.ca/') @Inject RxStreamingHttpClient httpClient

    String fetchMetadata(String metadataResource, String metadataFileName) {
        log.info "Fetching metadataResource: $metadataResource"
        log.trace "metadataFileName: $metadataFileName"
        HttpRequest<?> request = HttpRequest.HEAD(metadataResource)
        request.contentType(MediaType.TEXT_PLAIN)
        HttpResponse<?> head = httpClient.toBlocking().exchange(request)
        String newMetadataEtag = head.headers.get(HttpHeaders.ETAG)
        log.info "newMetadataEtag: ${newMetadataEtag}"

        request = HttpRequest.GET(metadataResource)
        request.contentType(MediaType.TEXT_PLAIN)
        Flowable<HttpResponse<ByteBuffer<?>>> responseFlowable =  httpClient.exchangeStream(
                request
        )
        File newFile = new File("/tmp/mdm/$metadataFileName")
        newFile.withOutputStream { stream ->
            responseFlowable.blockingForEach { HttpResponse<ByteBuffer<?>> response ->
                java.nio.ByteBuffer byteBuffer = response.body().asNioBuffer()
                stream.write(byteBuffer.array())
                ((ReferenceCounted)byteBuffer).release()
            }
        }

        log.info "newFile length: ${newFile.length()}"
        newMetadataEtag
    }

}