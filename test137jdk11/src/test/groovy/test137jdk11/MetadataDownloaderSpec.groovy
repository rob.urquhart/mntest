package test137jdk11

import io.micronaut.context.ApplicationContext
import spock.lang.Shared
import spock.lang.Specification


class MetadataDownloaderSpec extends Specification {

    @Shared MetadataDownloader metadataDownloader = ApplicationContext.run().getBean(MetadataDownloader)

    void 'test download from caf'() {
        when:
        metadataDownloader.fetchMetadata('CoreServices/caf_metadata_signed_sha256.xml', 'caf.xml')

        then:
        noExceptionThrown()
        new File('/tmp/mdm/caf.xml').exists()
        new File('/tmp/mdm/caf.xml').size() > 1100000

        cleanup:
        new File('/tmp/mdm/caf.xml').delete()
    }
}
